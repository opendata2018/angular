var app = angular.module('tozahavo', ['ui-leaflet', "chart.js"]);

Array.prototype.last = function () {
    return this[this.length - 1];
};

app.factory("interceptors", [function () {

    return {

        // if beforeSend is defined call it
        'request': function (request) {

            if (request.beforeSend)
                request.beforeSend();

            return request;
        },


        // if complete is defined call it
        'response': function (response) {

            if (response.config.complete)
                response.config.complete(response);

            return response;
        }
    };

}]);

app.config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push('interceptors');
}]);

angular.module('tozahavo').controller('main', function ($scope, $http) {
    var tilesDict = {
        google: {
            url: "http://mt0.google.com/vt/lyrs=s&x={x}&y={y}&z={z}"
        },

        analyzed: {
            url: "http://192.168.12.150:8000/map/{z}/{x}/{y}"
        }
    };

    var map;
    var zoomer;
    var selectedRect;


    //$scope.rectPush = [];
    $scope.reactItems = [];
    $scope.rectArray = [];

    $scope.colors = [];

    $scope.curColor = "#09f";

    $scope.colors_new = false;
    $scope.arraySelect = false;
    $scope.newAdd = false;
    $scope.chartShow = false;

    $scope.rects = [];

    $scope.closeFunc = function () {
        $scope.showInfo = false;

        $scope.rects.forEach(function (value) {
            map.removeLayer(value);
        });

        $scope.removeSelectedRect();
        $scope.chartShow = false;
    };

    $scope.removeSelectedRect = function () {
        if (selectedRect != undefined)
            map.removeLayer(selectedRect);
    };

    $scope.addRect = function () {
        if ($scope.newAdd) {
            $scope.rectArray.push($scope.reactItems);
            $scope.reactItems = [];
        } else {
            $scope.rectArray.push($scope.reactItems);
        }

        $scope.changeColor()
    };

    $scope.changeColor = function () {
        $scope.curColor = Chart.defaults.global.colors[$scope.rectArray.length];
    };

    $scope.endSelected = function () {
        $scope.addRect();
        selected_array_post()
        $scope.chartShow = true;
    };

    $scope.buttonAdd = function () {
        //console.log('dataaaaa')

        $scope.removeSelectedRect();
        $scope.rectArray = [];
        $scope.changeColor();

        $scope.arraySelect = true;
        $scope.newAdd = true;

        $scope.rects.forEach(function (value) {
            map.removeLayer(value);
        });
    };

    angular.extend($scope, {
        satellite: true,
        center: {
            lat: 41.309604,
            lng: 69.241050,
            zoom: 13
        },
        defaults: {
            scrollWheelZoom: false
        },
        tiles: {
            'url': tilesDict.google.url
        },

        events: {
            map: {
                enable: ['click', 'load'],
                logic: 'emit'
            }
        },

        areaInfo: {},
        compareData: {},

        datasetOverride: {
            backgroundColor: [],
        },

        chart: {
            options: {
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                            // OR //
                            beginAtZero: true   // minimum value will be 0.
                        }
                    }]
                }
            },
            labels: ['Площадь деревьев'],
            data: [
                [65],
                [28]
            ],
            series: ['Регион 1', 'Регион 2'],
            // colors: ["#ff8a80", "#ea80fc", "#8c9eff", "#80d8ff", "#a7ffeb", "#ccff90", "#ffff8d", "#6200ea"],
        }
    });

    $scope.$on('leafletDirectiveMap.load', function (event, args) {
        args.leafletObject.zoomControl._container.remove();

        zoomer = L.control.zoom({
            position: 'bottomleft'
        }).addTo(args.leafletObject);
    });


    $scope.$on('leafletDirectiveMap.click', function (e, args) {
        var latlng = args.leafletEvent.latlng;
        var mymap = args.leafletEvent.target;
        map = mymap;

        var pixelPoint = mymap.project(latlng, mymap.getZoom()).floor();
        var coords = {x: Math.floor(pixelPoint.x / 256), y: Math.floor(pixelPoint.y / 256)};
        coords.z = mymap.getZoom();

        if (mymap.getZoom() < 1) {
            return
        }

        //var img = mymap._layers[26]._tiles[coords.x + ":" + coords.y + ":" + coords.z].el;
        function tile2long(x, z) {
            return (x / Math.pow(2, z) * 360 - 180);
        }

        function tile2lat(y, z) {
            var n = Math.PI - 2 * Math.PI * y / Math.pow(2, z);
            return (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
        }

        var bounds = [
            [tile2lat(coords.y, coords.z), tile2long(coords.x, coords.z)],
            [tile2lat(coords.y + 1, coords.z), tile2long(coords.x + 1, coords.z)]
        ];

        if ($scope.arraySelect == false) {
            $scope.rectOneData = {x: coords.x, y: coords.y, z: coords.z, lat: latlng.lat, lng: latlng.lng}
            selected_one();
        } else {
            $scope.rects.push(L.rectangle(bounds, {color: $scope.curColor, weight: 1}).addTo(mymap));
            $scope.reactItems.push({x: coords.x, y: coords.y, z: coords.z, lat: latlng.lat, lng: latlng.lng});
        }

        $scope.removeSelectedRect();

        selectedRect = L.rectangle(bounds, {color: $scope.curColor, weight: 1}).addTo(mymap);
    });

    $scope.showInfo = false;
    $scope.showInfoLoading = true;

    function getRandomColor() {
        var num = Math.round(0xffffff * Math.random());
        var r = num >> 16;
        var g = num >> 8 & 255;
        var b = num & 255;
        return 'rgb(' + r + ', ' + g + ', ' + b + ')';
    }

    function selected_one() {
        $http({
            method: "POST",
            url: 'http://192.168.12.150:8000/loadByXYZ',
            data: $scope.rectOneData,
            beforeSend: function () {
                $scope.showInfo = true;
                $scope.showInfoLoading = true;
            }
        }).then(function mySuccess(response) {
            var data = response.data;

            $scope.showInfoLoading = false;
            $scope.areaInfo = data;
        }, function myError(data) {
            console.log('An error occurred.');
            console.log(data);
        });
    }

    $scope.$watch("arraySelect", function (val) {
        if (zoomer == undefined) return;

        if (val) {
            zoomer._container.style.display = 'none';
        } else {
            zoomer._container.style.display = 'block';
        }
    });

    $scope.$watch("satellite", function (sat) {
        $scope.tiles.url = !sat ? tilesDict.analyzed.url : tilesDict.google.url;
    });

    $scope.showChart = function () {
        var per = $scope.compareData.percentage;

        $scope.chart.colors = $scope.colors;

        $scope.chart.data = per.map(function (x) {

            return [x.totalTree];
        });

        debugger

        $scope.chart.series = Object.keys(per).map(function (value) {
            return "Регион " + (parseInt(value) + 1);
        });

    };

    function selected_array_post() {
        $http({
            method: "POST",
            url: 'http://192.168.12.150:8000/loadAllByXYZ',
            data: $scope.rectArray,

            beforeSend: function () {
                $scope.showInfoLoading = true;
            }
        }).then(function mySuccess(response) {
            var data = response.data;


            $scope.compareData = data;
            $scope.showInfoLoading = false;

            $scope.arraySelect = false;
            $scope.newAdd = false;

            $scope.showChart()
        }, function myError(data) {
            console.log('An error occurred.');
            console.log(data);
        });
    }

})
;